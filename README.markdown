## LinusTypes

My son, Linus, frequently wants to type on my computer.

The problem with this is that there's not a good spot for him to type. Sometimes he types over top of my work, and that is frustrating. I can open a text editor for him, but it's frustrating as well because the font size isn't adjustable so easily and you can't make the font pretty colors.

So I initially thought about making a site for him that he could type in, but that was a lot of work for something I only use infrequently (and I'd rather he not be in front of the screen for very long). The perfect solution wound up being the `content-editable` HTML attribute.

This script is meant to be used as a bookmarklet. Create a new bookmark, edit the url, and replace it with `javascript:` plus the content of the file. Then open a brand new `about:blank` tab, and click the bookmarklet.

It'll do the following:

1. Set the content editable.
2. Set the font to be quite large.
3. Prevent repeated keystroke: Linus likes to see the letters he types but he hasn't grokked the notion that you have to let go of the key quickly or it will write a million 'G's. This helps him see the letter from his key press but also helps him be able to see letters in comparison to each other.
