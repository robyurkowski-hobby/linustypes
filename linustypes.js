window.addEventListener("keydown", onKeyDown, false);
window.addEventListener("keyup", onKeyUp, false);

var lastKeyPressed;


function onKeyDown(event) {
  if (!lastKeyPressed) {
    lastKeyPressed = event.keyCode;
    console.log("Pressed", event.keyCode);
  }

  else {
    event.preventDefault();
    return false;
  }
}

function onKeyUp(event) {
  lastKeyPressed = false;
}


function setEditable() {
  document.getElementsByTagName('html')[0].contentEditable = true;
}


function setStyles() {
  var b = document.getElementsByTagName('body')[0];
  b.style.fontSize = "200%";
  b.style.fontWeight = "bold";
}

setEditable();
setStyles();
